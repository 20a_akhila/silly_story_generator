var CUSTOM_NAME = document.getElementById('customName');
var RANDOMIZE = document.querySelector('.randomize');
var STORY = document.querySelector('.story');
var RAdIO_GROUP = document.querySelectorAll('input[name="country"]');


function randomValurFromArray(arr){
    return arr[Math.floor(Math.random()*arr.length)];
}

var storyText = 'It was 94 fahrenheit outside,so :insertx: went for a walk. When they got to :inserty:, they stared in horror for a few moments, then :insertz:. Bob saw the whole thing, but was not surprised — :insertx: weighs 300 pounds, and it was a hot day.';
var insertX = ['Willy the Goblin', 'Big Daddy', 'Father Christmas'];
var insertY = ['the soup kitchen', 'Disneyland', 'the White House'];
var insertZ = ['spontaneously combusted', 'melted into a puddle on the sidewalk', 'turned into a slug and crawled away'];

RANDOMIZE.addEventListener('click', result);

function result() {
    var newStory = storyText;
    var xItem = randomValurFromArray(insertX);
    var yItem = randomValurFromArray(insertY);
    var zItem = randomValurFromArray(insertZ);
    
    var newStory = newStory.replace(':insertx:', xItem);
    var newStory = newStory.replace(':insertx:', xItem);
    var newStory = newStory.replace(':inserty:', yItem);
    var newStory = newStory.replace(':insertz:', zItem);

    if(CUSTOM_NAME.value !== '') {
        let name = CUSTOM_NAME.value;
        newStory = newStory.replace('Bob', name);
    }
  
    for(let i of RAdIO_GROUP){
      i.addEventListener('change',(e)=>{
        let selectedValue = e.target;
    if(selectedValue.id === 'uk') {
      var weight = Math.round(300*0.071429)+ 'stones';
      var temperature =  Math.round((94-32)/1.8) + 'centigrade';
      newStory = newStory.replace('300 pounds', weight);
      newStory = newStory.replace('94 farenheit', temperature);
  
    }
  })
  }
    STORY.textContent = newStory;
    STORY.style.visibility = 'visible';
  }

 